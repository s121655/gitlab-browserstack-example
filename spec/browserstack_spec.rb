require "rubygems"
require "selenium-webdriver"

# Input capabilities
caps = Selenium::WebDriver::Remote::Capabilities.new
caps["os"] = "Windows"
caps["os_version"] = "10"
caps["browser"] = "Chrome"
caps["browser_version"] = "72.0"
caps["javascriptEnabled"] = "true"

driver = Selenium::WebDriver.for(:remote,
  :url => "http://manikandanbalasu1:bRCf1qHr6RHnE4cLdNqi@hub-cloud.browserstack.com/wd/hub",
  :desired_capabilities => caps)

# Searching for 'BrowserStack' on google.com
driver.navigate.to "http://www.google.com"
element = driver.find_element(:name, "q")
element.send_keys "BrowserStack"
element.submit

puts driver.title

# Setting the status of test as 'passed' or 'failed' based on the condition; if title of the web page matches 'BrowserStack - Google Search'
if driver.title=="BrowserStack - Google Search"
  driver.execute_script('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Title matched!"}}')
else
  driver.execute_script('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Title not matched"}}')
end

driver.quit
